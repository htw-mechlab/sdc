# Getting Started 


## Install jupyter notebook
To install jupyter notebook please use the following links  <br>
https://jupyter.org/install.html
<br>

## Install Gitlab
An installation is not required. You can download the zip / tar.gz archive from the repositiory 

### Repository Download Path :
https://gitlab.com/htw-mechlab/sdc


